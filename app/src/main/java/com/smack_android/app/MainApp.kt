package com.smack_android

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.interceptors.HttpLoggingInterceptor
import com.facebook.stetho.Stetho
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.smack_android.utils.log_TEMP
import okhttp3.OkHttpClient

class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()

        //ShardPref Init
        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)


        // Stetho Interceptor Init
        Stetho.initializeWithDefaults(this)

        // Fast Android Network Init
        AndroidNetworking.initialize(applicationContext)

        // Normal Interceptor
//        AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY)

        // Stetho Interceptor for Fast
        val okHttpClient = OkHttpClient().newBuilder()
            .addNetworkInterceptor(StethoInterceptor())
            .build()
        AndroidNetworking.initialize(applicationContext, okHttpClient)
        //Gson Init
        val gsonBuilder = GsonBuilder()
        gson = gsonBuilder.create()

        instance = this

    }



    fun getContext(): Context {
        return applicationContext
    }

    fun getPref(): SharedPreferences {
        return sharedPreferences
    }
    fun getGson() : Gson {
        return gson
    }

    companion object {

        private lateinit var sharedPreferences: SharedPreferences
                private lateinit var  gson: Gson
        lateinit var instance: MainApp
            private set
    }

}