package com.smack_android.ui.activity.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.text.Layout
import android.view.Gravity
import android.widget.Toast
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.smack_android.R
import com.smack_android.data.network.helper.api_helper
import com.smack_android.data.network.model.Login_body
import com.smack_android.data.network.model.Login_response
import com.smack_android.data.pref.PrefHelper
import com.smack_android.enums.LoginStates
import com.smack_android.extentions.ext_gone
import com.smack_android.extentions.ext_visible
import com.smack_android.ui.activity.home.HomeActivity
import com.smack_android.ui.activity.register.RegisterActivity
import com.smack_android.utils.common_TEMP
import com.smack_android.utils.keyboard_TEMP
import com.smack_android.utils.log_TEMP
import com.smack_android.utils.snackbar_TEMP
import kotlinx.android.synthetic.main.activity_login_content.*
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {

    lateinit var context: Activity
    private lateinit var mEmail: String
    private lateinit var mPassword: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setUpViews()


    }

    private fun setUpViews() {
        ivClose.setOnClickListener {
            finish()
        }
        tvSignUp.setOnClickListener {
            startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
        }
        bLogin.setOnClickListener {
            LoginCheck()
        }
    }



    private fun LoginCheck() {
        keyboard_TEMP.hideSoftInput(this)
        mEmail = etEmail.text.toString()
        mPassword = etPassword.text.toString()
        if (mEmail == ("") || mPassword == ("")) {
            snackbar_TEMP.showCustomSnackbar(
                llLogin, "You Must Fill All Data", resources.getColor(R.color.colorSky),
                Gravity.TOP, resources.getColor(R.color.colorWhite)
            )
        } else if (!mEmail.matches(Regex("(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"))) {
            snackbar_TEMP.showCustomSnackbar(
                llLogin, "You Must Enter a Real Email", resources.getColor(R.color.colorSky),
                Gravity.TOP, resources.getColor(R.color.colorWhite)
            )
        } else {
            LoginService()

        }
    }

    private fun LoginService() {
        aviProgressLogin.ext_visible()
        api_helper.LoginRequest(Login_body(mEmail, mPassword))
            ?.getAsObject(Login_response::class.java, object : ParsedRequestListener<Login_response> {
                override fun onResponse(response: Login_response?) {
                    aviProgressLogin.ext_gone()
                    if (response?.user == null) {

                    } else {
                        Toast.makeText(this@LoginActivity, getString(R.string.LoginSuccess), Toast.LENGTH_SHORT).show()
                        PrefHelper.userLoginStates = LoginStates.lOGIN
                        PrefHelper.Mail = response?.user
                        PrefHelper.Token = response?.token
                        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    }
                }

                override fun onError(anError: ANError?) {
                    aviProgressLogin.ext_gone()
                    if (anError?.getErrorCode() == 401) {
                        val jsonObject = JSONObject(anError?.getErrorBody())
                        val realMessage = jsonObject.getString("message")

                        snackbar_TEMP.showCustomSnackbar(
                            llLogin, realMessage, common_TEMP.getColorCompat(R.color.colorSky),
                            Gravity.TOP, Layout.Alignment.ALIGN_CENTER.ordinal
                        )
                    }

                }

            })
    }
}
