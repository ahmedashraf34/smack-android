package com.smack_android.ui.activity.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity;
import com.smack_android.R
import com.smack_android.ui.activity.home.HomeActivity
import com.smack_android.utils.activity_TEMP

class SplashActivity : AppCompatActivity() {
    private val SPLASH_DISPLAY_LENGTH = 1500
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity_TEMP.fullScreenActivity(this)
        setContentView(R.layout.activity_splash)

        SplashScreenAction()
    }

    fun SplashScreenAction() {
        Handler().postDelayed({
            val mainIntent = Intent(this@SplashActivity, HomeActivity::class.java)
            this@SplashActivity.startActivity(mainIntent)
            this@SplashActivity.finish()
        }, SPLASH_DISPLAY_LENGTH.toLong())
    }

}
