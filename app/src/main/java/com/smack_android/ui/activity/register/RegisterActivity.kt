package com.smack_android.ui.activity.register

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.text.Layout
import android.view.Gravity
import android.widget.Toast
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.androidnetworking.interfaces.StringRequestListener
import com.smack_android.R
import com.smack_android.RequestAvatersId
import com.smack_android.data.network.helper.api_helper
import com.smack_android.data.network.model.*
import com.smack_android.data.pref.PrefHelper
import com.smack_android.enums.LoginStates
import com.smack_android.extentions.ext_gone
import com.smack_android.extentions.ext_visible
import com.smack_android.ui.activity.avaters.AvatersActivity
import com.smack_android.ui.activity.home.HomeActivity
import com.smack_android.utils.common_TEMP
import com.smack_android.utils.keyboard_TEMP
import com.smack_android.utils.log_TEMP
import com.smack_android.utils.snackbar_TEMP

import kotlinx.android.synthetic.main.activity_register_content.*
import org.json.JSONObject
import java.util.*

class RegisterActivity : AppCompatActivity() {


    private var cA: Int = 0
    private var cR: Int = 0
    private var cG: Int = 0
    private var cB: Int = 0
    private var colorArgb: Int = 0
    private lateinit var mUserName: String
    private lateinit var mEmail: String
    private lateinit var mPassword: String
    private var colorHexa: String = ""
    private var mNameExtra: String? = null
    private var mImageExtra: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        setUpViews()
    }

    private fun setUpViews() {
        //ChangeBackGround
        tvGenrateBackground.setOnClickListener {
            changeBackGround()
        }

        //Back
        ivClose2.setOnClickListener {
            finish()
        }

        //Register
        bRegister.setOnClickListener {
            registerCheck()
        }

        //ChoosePhoto
        tvChooseAvatar.setOnClickListener {
            startActivityForResult(Intent(this@RegisterActivity, AvatersActivity::class.java), RequestAvatersId)
        }
    }


    private fun changeBackGround() {
        val rnd = Random()
        cA = 255
        cR = rnd.nextInt(256)
        cG = rnd.nextInt(256)
        cB = rnd.nextInt(256)
        colorArgb = Color.argb(cA, cR, cG, cB)
        colorHexa = "[${cR / 255f},${cG / 255f},${cB / 255f},${cA / 255f}]"
        ivBackground.circleBackgroundColor = colorArgb
    }

    private fun registerCheck() {
        keyboard_TEMP.hideSoftInput(this)
        mUserName = etRegisterUser.text.toString()
        mEmail = etRegisterEmail.text.toString()
        mPassword = etRegisterPassword.text.toString()
        if (mEmail == ("") || mPassword == ("") || mUserName == ("") || mImageExtra == null || colorHexa == "") {
            snackbar_TEMP.showCustomSnackbar(
                llRegister, getString(R.string.fillAllData), common_TEMP.getColorCompat(R.color.colorSky),
                Gravity.TOP, Layout.Alignment.ALIGN_CENTER.ordinal
            )
        } else if (!mEmail.matches(Regex("(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"))) {
            snackbar_TEMP.showCustomSnackbar(
                llRegister, getString(R.string.checkEmail), common_TEMP.getColorCompat(R.color.colorSky),
                Gravity.TOP, Layout.Alignment.ALIGN_CENTER.ordinal
            )
        } else {
            registerService()
        }
    }

    private fun registerService() {
        aviProgressRegister.ext_visible()
        api_helper.RegisterRequest(Register_body(mEmail, mPassword))?.getAsString(object : StringRequestListener {
            override fun onResponse(response: String?) {
                if (response == "Successfully created new account") {
                    aviProgressRegister.ext_gone()
                    loginService()
                } else {

                }
            }

            override fun onError(anError: ANError?) {
                log_TEMP.NTest1("Register Error->>>>>\n" + anError + "\n" + anError?.getErrorCode() + "\n" + anError?.getErrorBody())
                aviProgressRegister.ext_gone()
                if (anError?.getErrorCode() == 500) {
                    val jsonObject = JSONObject(anError?.getErrorBody())
                    val message = jsonObject.getJSONObject("message")
                    val realMessage = message.getString("message")
                    snackbar_TEMP.showCustomSnackbar(
                        llRegister, realMessage, common_TEMP.getColorCompat(R.color.colorSky),
                        Gravity.TOP, Layout.Alignment.ALIGN_CENTER.ordinal
                    )
                }else{
                    snackbar_TEMP.showCustomSnackbar(
                        llRegister, getString(R.string.Error), common_TEMP.getColorCompat(R.color.colorSky),
                        Gravity.TOP, Layout.Alignment.ALIGN_CENTER.ordinal
                    )
                }
            }
        })
    }

    private fun loginService() {
        aviProgressRegister.ext_visible()
        api_helper.LoginRequest(Login_body(mEmail, mPassword))
            ?.getAsObject(Login_response::class.java, object : ParsedRequestListener<Login_response> {
                override fun onResponse(response: Login_response?) {
                    aviProgressRegister.ext_gone()
                    if (response?.user == null) {

                    } else {
                        PrefHelper.Mail = response.user
                        PrefHelper.Token = response.token
                        addUserService()
                    }
                }

                override fun onError(anError: ANError?) {
                    aviProgressRegister.ext_gone()


                    snackbar_TEMP.showCustomSnackbar(
                        llRegister, getString(R.string.Error), common_TEMP.getColorCompat(R.color.colorSky),
                        Gravity.TOP, Layout.Alignment.ALIGN_CENTER.ordinal
                    )
                }

            })
    }

    private fun addUserService() {
        aviProgressRegister.ext_visible()
        api_helper.AddUserRequest(AddUser_body(colorHexa, mNameExtra!!, mEmail, mUserName))
            ?.getAsObject(AddUser_response::class.java, object : ParsedRequestListener<AddUser_response> {
                override fun onResponse(response: AddUser_response?) {
                    aviProgressRegister.ext_gone()
                    Toast.makeText(this@RegisterActivity, getString(R.string.RegisterSuccess), Toast.LENGTH_SHORT)
                        .show()
                    PrefHelper.userLoginStates = LoginStates.lOGIN
                    val intent = Intent(this@RegisterActivity, HomeActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }

                override fun onError(anError: ANError?) {
                    aviProgressRegister.ext_gone()
                    snackbar_TEMP.showCustomSnackbar(
                        llRegister, getString(R.string.Error), common_TEMP.getColorCompat(R.color.colorSky),
                        Gravity.TOP, Layout.Alignment.ALIGN_CENTER.ordinal
                    )
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                RequestAvatersId -> {
                    mImageExtra = data?.getIntExtra("image", R.drawable.profiledefault)
                    mNameExtra = data?.getStringExtra("name")
                    ivBackground.setImageResource(mImageExtra!!)
                }
            }
        }
    }

}

