package com.smack_android.ui.activity.avaters

import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager
import com.smack_android.R
import com.smack_android.extentions.ext_setUpHolder
import com.smack_android.ui.placeholders.AvatersPlaceHolderView

import kotlinx.android.synthetic.main.activity_avaters_content.*
import kotlinx.android.synthetic.main.activity_register_content.*

class AvatersActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_avaters)

        setUpViews()


    }

    private fun setUpViews() {
        phvAvatars.ext_setUpHolder(GridLayoutManager(this@AvatersActivity,4))
        tvDark.setTextColor(resources.getColor(R.color.colorWhite))
        tvDark.setBackgroundColor(resources.getColor(R.color.colorBlue))
        tvLight.setTextColor(resources.getColor(R.color.colorDarkSky))
        tvLight.setBackgroundColor(resources.getColor(android.R.color.transparent))
        LoadDarkAvaters()

        tvDark.setOnClickListener {
            tvDark.setTextColor(resources.getColor(R.color.colorWhite))
            tvLight.setTextColor(resources.getColor(R.color.colorDarkSky))
            tvDark.setBackgroundColor(resources.getColor(R.color.colorBlue))
            tvLight.setBackgroundColor(resources.getColor(android.R.color.transparent))
            phvAvatars.removeAllViews()
            LoadDarkAvaters()

        }
        tvLight.setOnClickListener {
            tvLight.setTextColor(resources.getColor(R.color.colorWhite))
            tvDark.setTextColor(resources.getColor(R.color.colorDarkSky))
            tvLight.setBackgroundColor(resources.getColor(R.color.colorBlue))
            tvDark.setBackgroundColor(resources.getColor(android.R.color.transparent))
            phvAvatars.removeAllViews()
            LoadLightAvaters()
        }

        ivBack.setOnClickListener {
            finish()
        }
    }

    private fun LoadDarkAvaters() {
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark0",R.drawable.dark0))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark1",R.drawable.dark1))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark2",R.drawable.dark2))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark3",R.drawable.dark3))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark4",R.drawable.dark4))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark5",R.drawable.dark5))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark6",R.drawable.dark6))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark7",R.drawable.dark7))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark8",R.drawable.dark8))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark9",R.drawable.dark9))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark10",R.drawable.dark10))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark11",R.drawable.dark11))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark12",R.drawable.dark12))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark13",R.drawable.dark13))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark14",R.drawable.dark14))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark15",R.drawable.dark15))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark16",R.drawable.dark16))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark17",R.drawable.dark17))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark18",R.drawable.dark18))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark19",R.drawable.dark19))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark20",R.drawable.dark20))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark21",R.drawable.dark21))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark22",R.drawable.dark22))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark23",R.drawable.dark23))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark24",R.drawable.dark24))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark25",R.drawable.dark25))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark26",R.drawable.dark26))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"dark27",R.drawable.dark27))

    }
    fun LoadLightAvaters(){

        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light0))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light1))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light2))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light3))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light4))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light5))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light6))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light7))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light8))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light9))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light10))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light11))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light12))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light13))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light14))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light15))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light16))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light17))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light18))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light19))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light20))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light21))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light22))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light23))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light24))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light25))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light26))
        phvAvatars.addView(AvatersPlaceHolderView(this@AvatersActivity,"light0",R.drawable.light27))

    }

}
