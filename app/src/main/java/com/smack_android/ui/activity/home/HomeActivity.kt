package com.smack_android.ui.activity.home

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity;
import com.smack_android.R

import kotlinx.android.synthetic.main.activity_home.*
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.github.nkzawa.emitter.Emitter
import com.google.gson.reflect.TypeToken
import com.smack_android.MainApp
import com.smack_android.interFace.SelectChannel
import com.smack_android.data.network.helper.api_helper
import com.smack_android.data.network.model.FindAllChannel_response
import com.smack_android.data.network.model.FindAllMessage_response
import com.smack_android.data.network.model.FindUserByEmail_response
import com.smack_android.data.pref.PrefHelper
import com.smack_android.data.socket.SocketHelper
import com.smack_android.extentions.*
import com.smack_android.ui.activity.login.LoginActivity
import com.smack_android.ui.dialogs.create_channel.CreateChannelDialog
import com.smack_android.ui.dialogs.profile.ProfileDialog
import com.smack_android.ui.placeholders.ChannelPlaceHolderView
import com.smack_android.ui.placeholders.ChattingPlaceHolderView
import com.smack_android.utils.common_TEMP
import com.smack_android.utils.keyboard_TEMP
import com.smack_android.utils.log_TEMP
import com.smack_android.utils.snackbar_TEMP
import kotlinx.android.synthetic.main.activity_home_content.*
import kotlinx.android.synthetic.main.menu_home.*
import org.json.JSONObject
import com.google.gson.Gson




class HomeActivity : AppCompatActivity(), SelectChannel {
    override fun onSelectChannel(name: String, channelId: String) {
        phvHome.removeAllViews()
        currentChannelId = channelId
        tvTilte.text = "#$name"
        FindAllMessages()
        dlHomeNavBar.ext_toggleDrawer(Gravity.LEFT)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setUpViews()
        CheckLogin()


        ivSend.setOnClickListener {
            keyboard_TEMP.hideSoftInput(this@HomeActivity)
            SocketHelper.NewMessage(etChatting.text.toString(), mUserId!!,currentChannelId!!, mName!!, mAvatarName!!, mAvatarColor!!)
            etChatting.setText("")
        }


    }

    private fun setUpViews() {
        phvChannels.ext_setUpHolder(LinearLayoutManager(this@HomeActivity, RecyclerView.VERTICAL, false))

        ivMenu.setOnClickListener {
            dlHomeNavBar.ext_toggleDrawer(Gravity.LEFT)
        }




        llHomeLogin.setOnClickListener {
            log_TEMP.NTest1("click")
            if (PrefHelper.userLoginStates.states) {
                if (mEmail != null) {
                    var mProfileDialog = ProfileDialog()
                    mProfileDialog.show(supportFragmentManager, "")
                    mProfileDialog.SendData(mEmail!!, mName!!, colorArgb!!, mAvatarName!!)
                }
            } else {
                startActivity(Intent(this@HomeActivity, LoginActivity::class.java))

            }
        }



        ivAddChannel.setOnClickListener {
            if (PrefHelper.userLoginStates.states) {
                CreateChannelDialog().show(fragmentManager, "")
            } else {
                snackbar_TEMP.showCustomSnackbar(
                    llHome, "You Must Log In First ", resources.getColor(R.color.colorSky),
                    Gravity.TOP, resources.getColor(R.color.colorWhite)
                )
            }
        }

        etChatting.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (etChatting.text.toString() != ""){
                    ivSend.ext_visible()
                    SocketHelper.StartType(mName!!,currentChannelId)
                }else{
                    ivSend.ext_invisible()
                    SocketHelper.StopType(mName!!,currentChannelId)
                }
            }

        })
    }

    private fun CheckLogin() {
        if (PrefHelper.userLoginStates.states) {
            SocketHelper.ConnectSocket()
            FindUserByEmail()
            LoadChannelData()
            SocketHelper.ChannelCreated(ChannelCreatedListener())
            SocketHelper.MessageCreated(MessageCreatedListener())
            SocketHelper.UserTypingUpdate(UserTypingUpdateListener())

        }
    }

    private fun UserTypingUpdateListener(): Emitter.Listener {
        return object : Emitter.Listener {
            override fun call(vararg args: Any?) {
                runOnUiThread(object : Runnable {
                    override fun run() {
                        var Names = ""
                        val mapObj = Gson().fromJson<HashMap<String, String>>(
                            args[0].toString(), object : TypeToken<HashMap<String, String>>() {

                            }.type
                        )
                        if (mapObj.size == 0 || mapObj.isEmpty()) {
                            pbTyping.ext_gone()
                            tvTyping.text = ""
                            tvTyping.ext_gone()
                        } else {
                            for ((Name, ChannelId) in mapObj) {
                                if (mName != Name) {
                                    if (currentChannelId == ChannelId) {
                                        if (Names == "") {
                                            Names = Name
                                        } else {
                                            Names = Names + "," + Name
                                        }
                                    }
                                }
                            }
                            if (Names != "") {
                                var typingText = Names + " Typing Message"
                                pbTyping.ext_visible()
                                tvTyping.text = typingText
                                tvTyping.ext_visible()
                            }
                        }
                    }
                })
            }

        }
    }

    private fun MessageCreatedListener(): Emitter.Listener {
        return object : Emitter.Listener {
            override fun call(vararg args: Any?) {
                runOnUiThread(object : Runnable {
                    override fun run() {
                        val channelId = args[2].toString()
                        if (channelId == currentChannelId){
                            val msgBody = args[0].toString()
                            val username = args[3].toString()
                            val useravater = args[4].toString()
                            val AvaterColor = args[5].toString()
                            val id = args[6].toString()
                            val time = args[7].toString()
                            phvHome.addView(
                                ChattingPlaceHolderView(
                                    this@HomeActivity, FindAllMessage_response(
                                        "+++", msgBody, id,
                                        channelId, username, useravater, AvaterColor, 0, time)
                                )
                            )
                            phvHome.scrollToPosition(phvHome.viewCount)
                        }

                    }

                })
            }

        }
    }


    fun ChannelCreatedListener(): Emitter.Listener {

        return object : Emitter.Listener {
            override fun call(vararg args: Any?) {
                runOnUiThread(object : Runnable {

                    override fun run() {
                        val channelName = args[0].toString()
                        val channelDescription = args[1].toString()
                        val channelId = args[2].toString()
                        phvChannels.addView(
                            ChannelPlaceHolderView(
                                this@HomeActivity,
                                FindAllChannel_response(0, channelId, channelDescription, channelName)
                            )
                        )
                    }

                })
            }

        }
    }


    override fun onDestroy() {
        super.onDestroy()
        SocketHelper.DisconnectChannelCreated()
        SocketHelper.DisconnectMessageCreated()
        SocketHelper.DisconnectSocket()
    }


    private fun FindAllMessages() {
        aviProgressHome.ext_visible()
        api_helper.FindAllMessage(currentChannelId)
            ?.getAsParsed(object : TypeToken<ArrayList<FindAllMessage_response>>() {},
                object : ParsedRequestListener<ArrayList<FindAllMessage_response>> {
                    override fun onResponse(response: ArrayList<FindAllMessage_response>?) {
                        aviProgressHome.ext_gone()

                        for (item in response!!) {
                            phvHome.addView(ChattingPlaceHolderView(this@HomeActivity, item))
                        }
                    }

                    override fun onError(anError: ANError?) {
                        aviProgressHome.ext_gone()

                    }

                })
    }

    private var mEmail: String? = null
    private var mName: String? = null
    private var mUserId: String? = null
    private var mAvatarName: String? = null
    private var mAvatarColor: String? = null

    private fun FindUserByEmail() {
        aviProgressHome.ext_visible()
        val mail = PrefHelper.Mail
        api_helper.FindUserByEmailsRequest(mail)?.getAsObject(FindUserByEmail_response::class.java,
            object : ParsedRequestListener<FindUserByEmail_response> {
                @SuppressLint("NewApi")
                override fun onResponse(response: FindUserByEmail_response?) {
                    aviProgressHome.ext_gone()

                    mEmail = response?.email
                    mName = response?.name
                    mUserId = response?.id
                    mAvatarColor = response?.avatarColor
                    mAvatarName = response?.avatarName

                    tvLogin.text = mName
                    ivLogin.setImageDrawable(common_TEMP.getDrawable(mAvatarName!!))
                    ivLogin.circleBackgroundColor = PutBackgroundColor(mAvatarColor)
                }

                override fun onError(anError: ANError?) {
                    aviProgressHome.ext_gone()

                }

            })

    }


    private var colorArgb: Int? = 0

    @RequiresApi(Build.VERSION_CODES.O)
    private fun PutBackgroundColor(mAvatarColor: String?): Int {
        log_TEMP.NTest1(mAvatarColor!!)
        var color = mAvatarColor
        var color1 = color?.replace("[", "")
        var mColorString = color1?.replace("]", "")
        val colorArr = mColorString?.split(",")
        var cR = colorArr!![0].toFloat()
        var cG = colorArr!![1].toFloat()
        var cB = colorArr!![2].toFloat()
        var cA = colorArr!![3].toFloat()
        var mColorArgb = Color.argb(cA, cR, cG, cB)
        colorArgb = mColorArgb
        return mColorArgb
    }


    private lateinit var currentChannelId: String

    private fun LoadChannelData() {
        aviProgressHome.ext_visible()
        api_helper.FindAllChannel()?.getAsParsed(object : TypeToken<ArrayList<FindAllChannel_response>>() {},
            object : ParsedRequestListener<ArrayList<FindAllChannel_response>> {
                override fun onResponse(response: ArrayList<FindAllChannel_response>?) {
                    aviProgressHome.ext_gone()
                    for (item in response!!) {
                        phvChannels.addView(ChannelPlaceHolderView(this@HomeActivity, item))
                    }
                    currentChannelId = response?.get(0).id
                    tvTilte.text = "#${response?.get(0).name}"
                    FindAllMessages()

                }

                override fun onError(anError: ANError?) {
                    aviProgressHome.ext_gone()

                }

            })
    }


}
