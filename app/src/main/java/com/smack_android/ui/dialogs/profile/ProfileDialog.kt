package com.smack_android.ui.dialogs.profile

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import com.smack_android.R
import com.smack_android.data.pref.PrefHelper
import com.smack_android.enums.LoginStates
import com.smack_android.ui.activity.splash.SplashActivity
import com.smack_android.utils.common_TEMP
import kotlinx.android.synthetic.main.dialog_profile.view.*

class ProfileDialog : DialogFragment() {



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_profile, container, false)
        view.tvUserName.text = mName
        view.tvEmail.text = mEmail
        view.ivProfileDialog.setImageDrawable(common_TEMP.getDrawable(mAvatarName))

        view.ivProfileDialog.circleBackgroundColor = mColor!!


        view.ivClose4.setOnClickListener {
            dismiss()
        }

        view.tvLogOut.setOnClickListener {
            PrefHelper.userLoginStates = LoginStates.lOGOUT
            activity!!.finish()
            startActivity(Intent(activity,SplashActivity::class.java))
        }



        return view
    }



    private lateinit var mEmail: String
    private lateinit var mName: String
    private var mColor: Int? = 0
    private lateinit var mAvatarName: String

    fun SendData(email: String, name: String, color: Int, avatarName: String) {
        mName=name
        mEmail = email
        mColor = color
        mAvatarName = avatarName
    }

}