package com.smack_android.ui.dialogs.create_channel

import android.app.DialogFragment
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.text.Layout
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.smack_android.R
import com.smack_android.data.socket.SocketHelper
import com.smack_android.utils.common_TEMP
import com.smack_android.utils.snackbar_TEMP
import kotlinx.android.synthetic.main.activity_login_content.*
import kotlinx.android.synthetic.main.dialog_create_channel.*
import kotlinx.android.synthetic.main.dialog_create_channel.view.*

class CreateChannelDialog : DialogFragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_create_channel, container, false)
        view.ivClose3.setOnClickListener {
            dismiss()
        }

        view.bCreateChannel.setOnClickListener {
            if (view.etChannelName.text.toString()==""||view.etDescribtion.text.toString()==""){
                snackbar_TEMP.showCustomSnackbar(llChannelCreate,getString(R.string.fillAllData),common_TEMP.getColorCompat(R.color.colorSky),
                    Gravity.TOP,Layout.Alignment.ALIGN_CENTER.ordinal)
            }else{
                dismiss()
                SocketHelper.NewChannel(view.etChannelName.text.toString(),view.etDescribtion.text.toString())
            }
        }



        return view

    }

}