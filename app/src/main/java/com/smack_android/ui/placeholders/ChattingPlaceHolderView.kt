package com.smack_android.ui.placeholders

import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import android.widget.TextView
import android.widget.Toast
import com.mindorks.placeholderview.annotations.*
import com.smack_android.R
import com.smack_android.data.network.model.FindAllMessage_response
import com.smack_android.utils.common_TEMP
import com.smack_android.utils.log_TEMP
import de.hdodenhof.circleimageview.CircleImageView

@NonReusable
@Animate(Animate.CARD_TOP_IN_DESC)
@Layout(R.layout.item_chatting)
class ChattingPlaceHolderView(var context: Activity,var ChatData :FindAllMessage_response){

    @Position
    @JvmField
    var position: Int = 0

    @View(R.id.tvChatName)
    lateinit var tvChatName: TextView

    @View(R.id.tvChatTime)
    lateinit var tvChatTime: TextView

    @View(R.id.tvChatTypting)
    lateinit var tvChatTypting: TextView

    @View(R.id.ivChatPhoto)
    lateinit var ivChatPhoto: CircleImageView


    @RequiresApi(Build.VERSION_CODES.O)
    @Resolve
    fun onResolved() {
        tvChatName.text = ChatData.userName
        tvChatTime.text = ChatData.timeStamp
        tvChatTypting.text=ChatData.messageBody
        val userAvatar = ChatData.userAvatar
        ivChatPhoto.setImageDrawable(common_TEMP.getDrawable(userAvatar!!))
        ivChatPhoto.circleBackgroundColor = PutBackgroundColor(ChatData.userAvatarColor)

    }

//    @Recycle
//    fun onRecycled(){
//
//    }

    @Click(R.id.clChatting)
    fun onChannelClick(){
        Toast.makeText(context,"postion = $position",Toast.LENGTH_SHORT).show()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun PutBackgroundColor(mAvatarColor: String?): Int {
        var color = mAvatarColor
        var color1 = color?.replace("[", "")
        var mColorString = color1?.replace("]", "")
        val colorArr = mColorString?.split(",")
        var cR = colorArr!![0].toFloat()
        var cG = colorArr!![1].toFloat()
        var cB = colorArr!![2].toFloat()
        var cA = colorArr!![3].toFloat()
        var mColorArgb = Color.argb(cA, cR, cG, cB)
        return mColorArgb
    }

}


