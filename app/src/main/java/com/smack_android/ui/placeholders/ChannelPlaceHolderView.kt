package com.smack_android.ui.placeholders

import android.app.Activity
import android.widget.TextView
import android.widget.Toast
import com.mindorks.placeholderview.annotations.*
import com.smack_android.R
import com.smack_android.data.network.model.FindAllChannel_response
import com.smack_android.data.pref.PrefHelper
import com.smack_android.interFace.SelectChannel

@NonReusable
@Animate(Animate.CARD_TOP_IN_DESC)
@Layout(R.layout.item_channel)
class ChannelPlaceHolderView(
    var context: Activity,
    var Channel: FindAllChannel_response){

    @Position
    @JvmField
    var position: Int = 0

    @View(R.id.tvChannelName)
    lateinit var tvChannelName: TextView

    @Resolve
    fun onResolved() {
        tvChannelName.text = "#${Channel.name}"
        PrefHelper.ChannelId= Channel.id


    }

    @Recycle
    fun onRecycled(){

    }

    @Click(R.id.tvChannelName)
    fun onChannelClick(){

        if (context is SelectChannel) {
            var listner: SelectChannel = context as SelectChannel
            listner.onSelectChannel(Channel.name,Channel.id)
        }
    }



}


