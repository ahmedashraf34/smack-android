package com.smack_android.ui.placeholders

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.widget.ImageView
import com.bumptech.glide.request.RequestOptions
import com.mindorks.placeholderview.annotations.*
import com.smack_android.R
import com.smack_android.extentions.ext_loadGlideImage

@NonReusable
@Animate(Animate.CARD_TOP_IN_DESC)
@Layout(R.layout.item_avaters)
class AvatersPlaceHolderView(var context: Activity, var name: String, var mImage: Int) {

    @Position
    @JvmField
    var position: Int = 0

    @View(R.id.ivAvatersPhoto)
    lateinit var AvatersPhoto: ImageView

    @Resolve
    fun onResolved() {
        AvatersPhoto.ext_loadGlideImage(mImage, RequestOptions())

    }

//    @Recycle
//    fun onRecycled() {
//
//    }

    @Click(R.id.ivAvatersPhoto)
    fun onImageClick() {
        val data = Intent()
        data.putExtra("image", mImage)
        data.putExtra("name", name)
        context.setResult(RESULT_OK, data)
        context.finish()
    }


}


