package com.smack_android.utils

import android.app.Activity
import android.os.Build
import android.view.View
import android.view.Window
import android.view.WindowManager

object activity_TEMP{

    fun restartActivity(activity: Activity){
        val intent = activity.intent
        activity.finish()
        activity.startActivity(intent)
    }

    fun restartActivityWithTransition(activity: Activity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val intent = activity.intent
            activity.finishAfterTransition()
            activity.startActivity(intent)
        }
    }


    fun fullScreenActivity(activity: Activity){
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE)
        activity.getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

}