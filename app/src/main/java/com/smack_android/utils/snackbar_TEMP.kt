package com.smack_android.utils

import android.graphics.Typeface
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.TextView

object snackbar_TEMP{
    fun showNormalSnackbar(view :View,msg : String){
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show()
    }

    fun showNormalSnackbarWithAction(view :View,msg : String,btnTxt : String,onClickListener: View.OnClickListener){
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG).setAction(btnTxt,onClickListener).show()
    }

    fun showCustomSnackbar(view :View, msg: String,color : Int ,gravity: Int,textAliagment : Int){
        val snack = Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
        val baseView = snack.getView()
        val tv = baseView.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        tv.setTextColor(color)
        tv.gravity = gravity
        tv.textAlignment = textAliagment
        snack.show()
    }

    fun showCustomSnackbar(view :View, msg: String,color : Int ,gravity: Int,textAliagment : Int,font : Typeface){
        val snack = Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
        val baseView = snack.getView()
        val tv = baseView.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        tv.setTextColor(color)
        tv.gravity = gravity
        tv.typeface = font
        tv.textAlignment = textAliagment
        snack.show()
    }
}