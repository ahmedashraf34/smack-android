package com.smack_android.utils

import android.util.Log

object log_TEMP{
    private val Tag1 = "NTest1"
    private val Tag2 = "NTest2"
    private val Tag3 = "NTest3"

    fun NTest1(msg : String){
        Log.i(Tag1,msg)
    }

    fun NTest2(msg : String){
        Log.i(Tag2,msg)
    }

    fun NTest3(msg : String){
        Log.i(Tag3,msg)
    }
}