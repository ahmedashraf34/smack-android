package com.smack_android.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.smack_android.MainApp

object keyboard_TEMP {

    fun hideSoftInput(activity: Activity) {
        var view = activity.getCurrentFocus()
        if (view == null) view = View(activity)
        val imm = activity
                .getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
    }

    fun toggleSoftInput() {
        val imm = MainApp.instance.getContext()
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }
}