package com.smack_android.utils

import android.content.Context
import android.net.ConnectivityManager
import com.smack_android.MainApp

object network_connection_TEMP{
    fun isNetworkConnected(): Boolean {
        val cm = MainApp.instance.getContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
}