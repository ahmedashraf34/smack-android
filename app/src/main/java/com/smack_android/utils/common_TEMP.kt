package com.smack_android.utils

import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import com.smack_android.MainApp

object common_TEMP{


    fun changeIconDrawableColor(res: Int, color: Int) {
        var drawable: Drawable? = null
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            drawable = MainApp.instance.getContext().getDrawable(res)
        }
        if (drawable != null) {
            drawable.mutate()
            drawable.setColorFilter(
                ContextCompat
                    .getColor(MainApp.instance.getContext(), color), PorterDuff.Mode.SRC_ATOP)
        }
    }

    fun getColorCompat(color: Int) = ContextCompat.getColor(MainApp.instance.getContext(), color)

    fun getDrawable(@DrawableRes id: Int) = ContextCompat.getDrawable(MainApp.instance.getContext(), id)

    fun getDrawable(name: String): Drawable {
        val context = MainApp.instance.getContext()
        val resourceId =
            context.getResources().getIdentifier(name, "drawable", MainApp.instance.getContext().getPackageName())
        return context.getResources().getDrawable(resourceId)
    }

}