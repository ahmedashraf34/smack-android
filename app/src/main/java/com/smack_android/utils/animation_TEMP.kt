package com.smack_android.utils

import android.app.Activity
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.util.Pair
import android.transition.ChangeBounds
import android.transition.Explode
import android.transition.Fade
import android.transition.Slide
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.smack_android.MainApp
import com.smack_android.enums.InterpolationsTypes
import java.util.*

object animation_TEMP {

    fun createFadeAnimation(duration : Long): Fade? {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val fadeTransition = Fade()
            fadeTransition.duration = duration
            return fadeTransition
        } else {
            TODO("VERSION.SDK_INT < LOLLIPOP")
            return null
        }
    }

    fun createSlideAnimation(slideEdge : Int , duration : Long): Slide? {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val slideTransition = Slide()
            slideTransition.slideEdge = slideEdge
            slideTransition.duration = duration
            return slideTransition
        } else {
            TODO("VERSION.SDK_INT < LOLLIPOP")
            return null
        }
    }

    fun createExplodeAnimation(duration : Long,interpolator: InterpolationsTypes): Explode? {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val explode = Explode()
            explode.duration = duration
            explode.interpolator = interpolator.interpolator
            return explode
        } else {
            TODO("VERSION.SDK_INT < LOLLIPOP")
            return null
        }
    }

    fun createChangeBoundsAnimation(duration : Long): ChangeBounds? {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val changeBounds = ChangeBounds()
            changeBounds.duration = duration

            return changeBounds
        } else {
            TODO("VERSION.SDK_INT < LOLLIPOP")
            return null
        }
    }


    fun animateViewsOut( viewGroup : ViewGroup){
        val loadInterpolator = AnimationUtils.loadInterpolator(MainApp.instance.getContext(), android.R.interpolator.linear_out_slow_in)
        for (i in 0 until viewGroup.getChildCount()) {
            val child = viewGroup.getChildAt(i)
            child.animate()
                .setStartDelay(i.toLong())
                .setInterpolator(loadInterpolator)
                .alpha(0f)
                .scaleX(0f)
                .scaleY(0f)
        }
    }

    fun animateViewsIn( viewGroup : ViewGroup){
        val loadInterpolator = AnimationUtils.loadInterpolator(MainApp.instance.getContext(), android.R.interpolator.linear_out_slow_in)
        for (i in 0 until viewGroup.getChildCount()) {
            val child = viewGroup.getChildAt(i)
            child.animate()
                .setStartDelay((100 + i * 100).toLong())
                .setInterpolator(loadInterpolator)
                .alpha(1f)
                .scaleX(1f)
                .scaleY(1f)
        }
    }

    fun animationBetweenActivitesXML(From: Activity, animResourceStart: Int, animResourceEnd: Int) {
        From.overridePendingTransition(animResourceStart, animResourceEnd)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    fun createSafeTransitionParticipants(activity: Activity,
                                         includeStatusBar: Boolean, vararg otherParticipants: Pair<View, String>
    ): Array<Pair<View, String>> {
        // Avoid system UI glitches as described here:
        // https://plus.google.com/+AlexLockwood/posts/RPtwZ5nNebb
        val decor = activity.window.decorView
        var statusBar: View? = null
        if (includeStatusBar) {
            statusBar = decor.findViewById(android.R.id.statusBarBackground)
        }
        val navBar = decor.findViewById<View>(android.R.id.navigationBarBackground)

        // Create pair of transition participants.
        val participants = ArrayList<Pair<View,String>>(3)
        addNonNullViewToTransitionParticipants(statusBar, participants)
        addNonNullViewToTransitionParticipants(navBar, participants)
        // only add transition participants if there's at least one none-null element
        if (otherParticipants != null && !(otherParticipants.size == 1 && otherParticipants[0] == null)) {
            participants.addAll(Arrays.asList<Pair<View, String>>(*otherParticipants))
        }
        return participants.toTypedArray<Pair<View, String>>()
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private fun addNonNullViewToTransitionParticipants(view: View?, participants: MutableList<Pair<View, String>>) {
        if (view == null) {
            return
        }
        participants.add(Pair(view, view.transitionName))
    }


    fun loadViewAnimationXml(view: View, animResource: Int): Animation {
        val anim = AnimationUtils.loadAnimation(MainApp.instance.getContext(), animResource)
        view.startAnimation(anim)
        return anim
    }


}