package com.smack_android.extentions

import android.support.v7.widget.RecyclerView
import com.mindorks.placeholderview.PlaceHolderView

fun PlaceHolderView.ext_setUpHolder(layout : RecyclerView.LayoutManager){
    this.builder
        .setHasFixedSize(false)
        .setItemViewCacheSize(10)
        .setLayoutManager(layout)
}
fun PlaceHolderView.ext_setUpHolder(layout : RecyclerView.LayoutManager,itemDecoration:  RecyclerView.ItemDecoration){
    this.builder
        .setHasFixedSize(false)
        .setItemViewCacheSize(10)
        .setLayoutManager(layout)
    this.addItemDecoration(itemDecoration)
}
fun PlaceHolderView.ext_refreshItemDecoration(itemDecoration:  RecyclerView.ItemDecoration){
    this.removeItemDecoration(itemDecoration)
    this.addItemDecoration(itemDecoration)
}