package com.smack_android.extentions

import android.content.Context
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import com.daimajia.androidanimations.library.YoYo
import com.smack_android.MainApp
import com.smack_android.enums.YoyoAnimationTypes


fun View.ext_animateYoYo(animation : YoyoAnimationTypes, duration: Long, onStart : YoYo.AnimatorCallback, onEnd : YoYo.AnimatorCallback) {
    val yoYo = YoYo.with(animation.animation)
    yoYo.duration(duration)
    yoYo.onStart(onStart)
    yoYo.onEnd(onEnd)
    yoYo.playOn(this)
}

fun View.ext_animateXML(animResource: Int): Animation {
    val anim = AnimationUtils.loadAnimation(MainApp.instance.getContext(), animResource)
    this.startAnimation(anim)
    return anim
}

fun View.ext_visible() : View {
    if (visibility != View.VISIBLE) {
        visibility = View.VISIBLE
    }
    return this
}


fun View.ext_invisible() : View {
    if (visibility != View.INVISIBLE) {
        visibility = View.INVISIBLE
    }
    return this
}


fun View.ext_gone() : View {
    if (visibility != View.GONE) {
        visibility = View.GONE
    }
    return this
}

fun View.ext_toggleVisibility() : View {
    if (visibility == View.VISIBLE) {
        visibility = View.INVISIBLE
    } else {
        visibility = View.INVISIBLE
    }
    return this
}

fun View.ext_showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    this.requestFocus()
    imm.showSoftInput(this, 0)
}


fun View.ext_hideKeyboard(): Boolean {
    try {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    } catch (ignored: RuntimeException) { }
    return false
}


