package com.smack_android.extentions

import android.graphics.Bitmap
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.smack_android.MainApp

fun ImageView.ext_loadGlideImage(url : String,options: RequestOptions){
    Glide.with(MainApp.instance.getContext()).load(url).transition(withCrossFade()).apply(options).into(this)
}

fun ImageView.ext_loadGlideImage(bitmapImage : Bitmap,options: RequestOptions){
    Glide.with(MainApp.instance.getContext()).load(bitmapImage).transition(withCrossFade()).apply(options).into(this)
}

fun ImageView.ext_loadGlideImage(drawbleResId : Int,options: RequestOptions){
    Glide.with(MainApp.instance.getContext()).load(drawbleResId).transition(withCrossFade()).apply(options).into(this)
}

fun ImageView.ext_loadGlideGifImage(gifResId : Int,options: RequestOptions){
    Glide.with(MainApp.instance.getContext())
            .asGif()
            .load(gifResId)
            .transition(withCrossFade())
            .apply(options)
            .into(this)
}
