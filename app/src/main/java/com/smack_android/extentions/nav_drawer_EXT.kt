package com.smack_android.extentions

import android.support.v4.widget.DrawerLayout

fun DrawerLayout.ext_toggleDrawer(gravity: Int){
    if (this.isDrawerOpen(gravity)){
        closeDrawer(gravity)
    }else{
        openDrawer(gravity)
    }
}