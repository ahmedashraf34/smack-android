package com.smack_android.enums

enum class LoginStates(var states : Boolean) {
    lOGIN(true),
    lOGOUT(false)
}