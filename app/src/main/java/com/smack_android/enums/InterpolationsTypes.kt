package com.smack_android.enums

import android.animation.TimeInterpolator
import android.view.animation.*

enum class InterpolationsTypes(var interpolator: TimeInterpolator) {
    anticipateOvershoot(AnticipateOvershootInterpolator()),
    overshootInterpolator(OvershootInterpolator()),
    linearInterpolator(LinearInterpolator()),
    bounceInterpolator(BounceInterpolator()),
    anticipateInterpolator(AnticipateInterpolator()),
    accelerateInterpolator(AccelerateInterpolator()),
    accelerateDecelerateInterpolator(AccelerateDecelerateInterpolator())
}