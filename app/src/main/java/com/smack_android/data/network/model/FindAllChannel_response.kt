package com.smack_android.data.network.model

import com.google.gson.annotations.SerializedName

data class FindAllChannel_response(
    @SerializedName("__v")
    var v: Int,
    @SerializedName("_id")
    var id: String,
    @SerializedName("description")
    var description: String,
    @SerializedName("name")
    var name: String
)