package com.smack_android.data.network.model

import com.google.gson.annotations.SerializedName

data class Login_body(
    @SerializedName("email")
    var email: String,
    @SerializedName("password")
    var password: String
)