package com.smack_android.data.network.model

import com.google.gson.annotations.SerializedName

data class AddUser_body(
    @SerializedName("avatarColor")
    var avatarColor: String,
    @SerializedName("avatarName")
    var avatarName: String,
    @SerializedName("email")
    var email: String,
    @SerializedName("name")
    var name: String
)