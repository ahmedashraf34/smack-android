package com.smack_android.data.network.model

import com.google.gson.annotations.SerializedName

data class FindAllMessage_response(
    @SerializedName("_id")
    var id: String?,
    @SerializedName("messageBody")
    var messageBody: String?,
    @SerializedName("userId")
    var userId: String?,
    @SerializedName("channelId")
    var channelId: String?,
    @SerializedName("userName")
    var userName: String?,
    @SerializedName("userAvatar")
    var userAvatar: String?,
    @SerializedName("userAvatarColor")
    var userAvatarColor: String?,
    @SerializedName("__v")
    var v: Int?,
    @SerializedName("timeStamp")
    var timeStamp: String?
)