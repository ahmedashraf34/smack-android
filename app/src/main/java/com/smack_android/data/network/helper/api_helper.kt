package com.smack_android.data.network.helper

import android.renderscript.RenderScript
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.ANRequest
import com.androidnetworking.common.Priority
import com.smack_android.data.network.helper.LOGIN_URL
import com.smack_android.data.network.model.AddUser_body
import com.smack_android.data.network.model.Login_body
import com.smack_android.data.network.model.Register_body
import com.smack_android.data.pref.PrefHelper
import okhttp3.OkHttpClient
import java.io.File

//Api Requests

object api_helper{

    //Web_Request ( - Register - )

    fun RegisterRequest (body: Register_body): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(REGISTER_URL)
            .addApplicationJsonBody(body)
            .setPriority(Priority.MEDIUM)
            .build()
    }

    //Web_Request ( - login - )

    fun LoginRequest (body: Login_body): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(LOGIN_URL)
            .addApplicationJsonBody(body)
            .setPriority(Priority.MEDIUM)
            .build()
    }

    //Web_Request ( - add User - )

    fun AddUserRequest (body: AddUser_body): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(ADD_USER_URL)
            .addApplicationJsonBody(body)
            .addHeaders("Authorization",PrefHelper.Token)
            .setPriority(Priority.MEDIUM)
            .build()
    }


    //Web_Request ( - All Channels - )

    fun FindAllChannel (): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.get(ALL_CHANNELS_URL)
            .addHeaders("Authorization",PrefHelper.Token)
            .setPriority(Priority.MEDIUM)
            .build()
    }


    //Web_Request ( - find user by email - )

    fun FindUserByEmailsRequest (email : String): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.get(FIND_USER_BY_EMAIL_URL)
            .addPathParameter("email",email)
            .addHeaders("Authorization",PrefHelper.Token)
            .setPriority(Priority.MEDIUM)
            .build()
    }


    //Web_Request ( - channel Messages - )

    fun FindAllMessage (channelId : String): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.get(CHANNEL_MESSAGE_URL)
            .addPathParameter("channelId",channelId)
            .addHeaders("Authorization",PrefHelper.Token)
            .setPriority(Priority.MEDIUM)
            .build()
    }

}