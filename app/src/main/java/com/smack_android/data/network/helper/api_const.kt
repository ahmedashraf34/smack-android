package com.smack_android.data.network.helper

//Api Constents

val BASE_URL = "https://chatiosapp.herokuapp.com/v1/"
val REGISTER_URL = BASE_URL + "account/register"
val LOGIN_URL = BASE_URL + "account/login"
val ADD_USER_URL = BASE_URL + "user/add"
val ALL_CHANNELS_URL = BASE_URL + "channel"
val FIND_USER_BY_EMAIL_URL = BASE_URL + "user/byEmail/{email}"
val CHANNEL_MESSAGE_URL = BASE_URL + "message/byChannel/{channelId}"
