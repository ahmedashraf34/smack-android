package com.smack_android.data.network.model

import com.google.gson.annotations.SerializedName

data class Login_response(
    @SerializedName("token")
    var token: String,
    @SerializedName("user")
    var user: String
)