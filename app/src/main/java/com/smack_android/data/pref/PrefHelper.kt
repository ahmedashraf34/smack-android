package com.smack_android.data.pref


import com.smack_android.MainApp
import com.smack_android.enums.LoginStates


object PrefHelper {

    //Shard ( - Login States - )

    var userLoginStates: LoginStates
        get() {
            val states = MainApp.instance.getPref().getBoolean(LOGIN_STATES_SHARD, false)
            when (states) {
                true -> return LoginStates.lOGIN
                false -> return LoginStates.lOGOUT
            }
        }
        set(value) {
            MainApp.instance.getPref().edit().putBoolean(LOGIN_STATES_SHARD, value.states).apply()
        }


    //Shard ( - Token - )

    var Token: String
        get() {
            return MainApp.instance.getPref().getString(TOKEN_SHARD, "")
        }
        set(value) {
            MainApp.instance.getPref().edit().putString( TOKEN_SHARD,"Bearer " + value).apply()
        }

    //Shard ( - Mail - )

    var Mail: String
        get() {
            return MainApp.instance.getPref().getString(Mail_SHARD, "")
        }
        set(value) {
            MainApp.instance.getPref().edit().putString( Mail_SHARD, value).apply()
        }


    var ChannelId: String
        get() {
            return MainApp.instance.getPref().getString("sss", "")
        }
        set(value) {
            MainApp.instance.getPref().edit().putString( "sss", value).apply()
        }
}