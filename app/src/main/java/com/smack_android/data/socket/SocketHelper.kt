package com.smack_android.data.socket

import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.smack_android.MainApp
import com.smack_android.WEBSITE_URL
import com.smack_android.data.network.helper.BASE_URL
import com.smack_android.utils.log_TEMP

object SocketHelper {

    private val mSocket: Socket? = IO.socket(WEBSITE_URL)

    fun ConnectSocket() {
        mSocket?.connect()
            ?.on(Socket.EVENT_CONNECT, { log_TEMP.NTest1("Socket Connected") })
            ?.on(Socket.EVENT_DISCONNECT, { log_TEMP.NTest1("Socket DisConnected") })
    }


    fun DisconnectSocket() {
        mSocket?.disconnect()
    }

    //-----------------------------------------------------------------------------------------------------

    // Add/Get Channel

    fun NewChannel(name: String, descraption: String) {
        mSocket?.emit("newChannel", name, descraption)
    }

    fun ChannelCreated(fn: Emitter.Listener) {
        mSocket?.on("channelCreated", fn)
    }

    fun DisconnectChannelCreated() {
        mSocket?.off("channelCreated")
    }


    // Send /  Receive Message

    fun NewMessage(messageBody: String, userId: String,channelId : String,userName: String,avatarName : String,avatarColor: String) {
        mSocket?.emit("newMessage", messageBody, userId,channelId,userName,avatarName,avatarColor)
    }

    fun MessageCreated(fn: Emitter.Listener) {
        mSocket?.on("messageCreated", fn)
    }

    fun DisconnectMessageCreated() {
        mSocket?.off("messageCreated")
    }


    // Start / Stop / Update Typing


    fun StartType(userName : String,channelId: String) {
        mSocket?.emit("startType",userName,channelId)
    }

    fun StopType(userName : String,channelId: String) {
        mSocket?.emit("stopType",userName,channelId)
    }

    fun UserTypingUpdate(fn: Emitter.Listener) {
        mSocket?.on("userTypingUpdate", fn)
    }

    fun DisconnectUserTypingUpdate() {
        mSocket?.off("userTypingUpdate")
    }

}